import { PolymerElement, html } from '@polymer/polymer';
import 'polymer-zen-progress/index.js';
import 'polymer-zen-menu/index.js';

class MyElement extends PolymerElement {


  static get template() {
    return html`
    <style> .title { color: green; } .xp { color: purple;   display: block;  float: left;   border: 1px solid purple;   } </style>
      <span class="title">Welcome to Zen!<br /></span>
      <span class="tooltip">Here is your progress</span><br />
      <zen-progress xp={{xp}} on-xp-changed="modifyXp"> on-click="click"</zen-progress></br>
      <span class="tooltip">Choose something from the menu</span><br />
      <zen-menu class="xp" xp={{xp}}></zen-menu>
    `;
  }

  modifyXp() {
    console.log("xp modified");
    let element = this.shadowRoot.querySelector('zen-menu');
    console.log(element);
    element.updateMenu();
  }

  click() {
  console.log("click");
  let element = this.shadowRoot.querySelector('zen-menu');
  console.log(element);
  element.updateMenu();
}

}

customElements.define('wc-zen', MyElement);
